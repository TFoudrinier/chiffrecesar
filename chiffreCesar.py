import string
from tkinter import * 
from tkinter.messagebox import *
frame = Tk()
chaineASCII = string.ascii_lowercase + string.ascii_uppercase

def buttonCryptage_callback():
    messageLabel = Label(frame, text="Votre message : ")
    messageLabel.grid(row=9, sticky='n')
    affichage(cryptage(texte.get(),int(texteDecalage.get())))

def cryptage(texteC, decalage): # décale les caractères en ajoutant leur valeur ASCII par un décalage et retourne ce string modifié
    listeMessageC = []
    for lettre in texteC:
        lettreCrypte = chr(ord(lettre) + decalage)
        listeMessageC.append(lettreCrypte)
    
    texteCrypte = "".join(listeMessageC)
    return texteCrypte

def buttonCryptageCirculaire_callback():
    messageLabel = Label(frame, text="Votre message : ")
    messageLabel.grid(row=9, sticky='n')
    affichage(cryptageCirculaire(texte.get(),int(texteDecalage.get())))    

def cryptageCirculaire(texteCCirculaire, decalage): # décale les caractères en ajoutant leur valeur ASCII par un décalage et retourne ce string modifié en circulaire
    listeMessageCCirculaire = []
    chaineASCIICrypte = chaineASCII[decalage:] + chaineASCII[:decalage]
    for lettre in texteCCirculaire:
        lettreCrypte = (chaineASCII.index(lettre))
        listeMessageCCirculaire.append(chaineASCIICrypte[lettreCrypte])
    
    texteCrypte = "".join(listeMessageCCirculaire)
    return texteCrypte            

    
    
def buttonDecryptage_callback():
    messageLabel = Label(frame, text="Votre message : ")
    messageLabel.grid(row=9, sticky='n')
    affichage(decryptage(texte.get(), int(texteDecalage.get())))

def decryptage(texteD, decalage): # décale les caractères en diminuant leur valeur ASCII par un décalage et retourne ce string modifié
    listeMessageD = []
    for lettre in texteD:
        lettreCrypte = chr(ord(lettre) - decalage)
        listeMessageD.append(lettreCrypte)
    texteDecrypte = "".join(listeMessageD)
    return texteDecrypte    

def buttonDecryptageCirculaire_callback(): # décale les caractères en diminuant leur valeur ASCII par un décalage et retourne ce string modifié en circulaire
    messageLabel = Label(frame, text="Votre message : ")
    messageLabel.grid(row=9, sticky='n')
    affichage(decryptageCirculaire(texte.get(),int(texteDecalage.get())))

def decryptageCirculaire(texteDCirculaire, decalage):
    listeMessageDCirculaire = []
    chaineASCIICrypte = chaineASCII[decalage:] + chaineASCII[:decalage]
    chaineASCIIDecrypte = chaineASCIICrypte[:decalage] + chaineASCIICrypte[decalage:]
    for lettre in texteDCirculaire:
        lettreDecrypte = (chaineASCIIDecrypte.index(lettre))
        listeMessageDCirculaire.append(chaineASCII[lettreDecrypte])
    
    texteDecrypte = "".join(listeMessageDCirculaire)
    return texteDecrypte  
         

def affichage(messageTraite):
    textLabel.set(messageTraite)

frame.rowconfigure(0, weight=2)
frame.columnconfigure(1, weight=2)

labelMessage = Label(frame, text="Rentrez le message :")
labelMessage.grid(row=0, sticky='n')

texte = StringVar() # message rentré par l'utilisateur 
texte.set("")
entry = Entry(frame, textvariable=texte, width=8)   
entry.grid(row=0, column=1, sticky='nw')

labelDecalage = Label(frame, text="Rentrez le décalage :")
labelDecalage.grid(row=0, column =2, sticky='n')

texteDecalage = StringVar() # décalage rentré par l'utilisateur
texteDecalage.set("")
entryDecalage = Entry(frame, textvariable=texteDecalage, width=8)    
entryDecalage.grid(row=0, column=3, sticky='nw')

buttonCryptageNonCirculaire = Button(frame, text="Cryptage non circulaire", command=buttonCryptage_callback, cursor="hand2") # bouton utilisé pour crypter le message
buttonCryptageNonCirculaire.grid(row=7, column=0, sticky='s')

buttonCryptageCirculaire = Button(frame, text="Cryptage circulaire", command=buttonCryptageCirculaire_callback, cursor="hand2") # bouton utilisé pour crypter le message de façon circulaire
buttonCryptageCirculaire.grid(row=8, column=0, sticky='s')

buttonDecryptage = Button(frame, text="Decryptage non circulaire", command=buttonDecryptage_callback, cursor="hand2") # bouton utilisé pour décrypter le message
buttonDecryptage.grid(row=7, column=2, sticky='s')

buttonDecryptageNonCirculaire = Button(frame, text="Decryptage circulaire", command=buttonDecryptageCirculaire_callback, cursor="hand2") # bouton utilisé pour décrypter le message de façon circulaire
buttonDecryptageNonCirculaire.grid(row=8, column=2, sticky='s')

textLabel = StringVar() # affichage du message crypté ou décrypté selon l'utilisateur
label = Label(frame, textvariable=textLabel)
label.grid(row=9, column=1, sticky='s')

frame.mainloop()
