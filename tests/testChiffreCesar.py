import unittest
from chiffreCesar import cryptage
from chiffreCesar import decryptage
from chiffreCesar import cryptageCirculaire
from chiffreCesar import decryptageCirculaire

class testCryptageEtDecryptage(unittest.TestCase):

    def testCryptage(self): # test lors du cryptage non circulaire 
        self.assertEqual(cryptage('abc', 8), 'ijk')
        self.assertEqual(cryptage('123', 8), '9:;')
        self.assertEqual(cryptage('1aà', 8), '9iè')

    def testDecryptage(self): # test lors du decryptage non circulaire
        self.assertEqual(decryptage('ijk', 8), 'abc')
        self.assertEqual(decryptage('123', 8), ')*+')
        self.assertEqual(decryptage('1a-', 8), ')Y%')

    def testCryptageCirculaire(self): # test lors du cryptage circulaire
        self.assertEqual(cryptageCirculaire('abc', 8), 'ijk')
        self.assertEqual(cryptageCirculaire('xyz', 3), 'ABC')
        self.assertEqual(cryptageCirculaire('XYZ', 8), 'fgh')

    def testDecryptageCirculaire(self): # test lors du decryptage circulaire
        self.assertEqual(decryptageCirculaire('ijk', 8), 'abc')
        self.assertEqual(decryptageCirculaire('abc', 3), 'XYZ')
        self.assertEqual(decryptageCirculaire('FGH', 8), 'xyz')
        
